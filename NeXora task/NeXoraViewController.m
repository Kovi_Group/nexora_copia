//
//  NeXoraViewController.m
//  NeXora task
//
//  Created by KOVI GROUP on 07/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//
#import "Model.h"
#import "NeXoraViewController.h"
#import "AFNetworking.h"
#import "UIView+AOAnimation.h"
#define WEBPHOTOS @"http://jsonplaceholder.typicode.com/photos"

@interface NeXoraViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSArray *NeXora;


@end

@implementation NeXoraViewController
@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self network];
     
}

/*=============================
 Check  the network
==============================*/
-(void)network
{
    [[AFNetworkReachabilityManager sharedManager]startMonitoring];
    [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
        if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
            
            //If Internet ok
            [self dataJesonFormUrl];
            
        }else{
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error No Internet"
                                                                           message:@"Check Your Internet Connection."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }];

    
}

/*=============================
//If network Get Data
==============================*/
-(void)dataJesonFormUrl
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:WEBPHOTOS parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        if (responseObject) {
            
            //filtre get the first 10 item
             self.NeXora = [responseObject subarrayWithRange:NSMakeRange(0, 10)];
            [self->tableView reloadData];
        }
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        NSLog(@"Fatal Error");
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"An error occurred try again."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }];

}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return[self.NeXora count];
}
/*=============================
//the cells have the same dimensions 150
 ==============================*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
/*=============================
//show on tableview
 ==============================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Model *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    //Get text
    NSArray *data = [self. NeXora objectAtIndex:indexPath.row];
    cell.NeXText.text = [data valueForKey:@"title"];

     //Get Imagen from Url
    
    NSString *urlString = [data valueForKey:@"thumbnailUrl"];;
    NSURLRequest *requestc = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [cell.NeXImage loadRequest:requestc];
    
    [cell.NeXImage aoAnimation:AOAnimationFlyInLeft option:UIViewAnimationOptionCurveEaseOut duration:0.7 delay:1.2 startScale:1 startAlpha:0 angle:0 valueOne:0 valueTwo:0];
    
      [cell.NeXText aoAnimation:AOAnimationFlyInRight option:UIViewAnimationOptionCurveEaseOut duration:0.7 delay:1.2 startScale:1 startAlpha:0 angle:0 valueOne:0 valueTwo:0];
    
    
    
    return cell;
}


@end
