//
//  ModelContent.h
//  NeXora task
//
//  Created by KOVI GROUP on 08/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModelContent : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIWebView *NeXImage;
@property (strong, nonatomic) IBOutlet UITextView *NeXText;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *m_activity;
    
@end

