//
//  main.m
//  NeXora task
//
//  Created by KOVI GROUP on 07/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
