//
//  HomeScreen.m
//  NeXora task
//
//  Created by KOVI GROUP on 08/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//

#import "HomeScreen.h"
#import "AFNetworking.h"
#import <AFNetworkReachabilityManager.h>
@interface HomeScreen ()

@end

@implementation HomeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self network];
    
    

}

    
/*=============================
Check  the network
==============================*/
-(void)network
{

    [[AFNetworkReachabilityManager sharedManager]startMonitoring];
    [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
    if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
        
        //If Internet ok

            }else{
                
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error No Internet"
                                                                               message:@"Check Your Internet Connection."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }];
}
    
@end
