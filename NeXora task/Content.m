//
//  Content.m
//  NeXora task
//
//  Created by KOVI GROUP on 08/12/2018.
//  Copyright © 2018 kovigroup. All rights reserved.
//
#import "ModelContent.h"
#import "Content.h"
#import "AFNetworking.h"
#import "UIView+AOAnimation.h"
#define WEBPHOTOS @"http://jsonplaceholder.typicode.com/photos"
@interface Content ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *MyCollection;
@property (nonatomic, retain) NSArray *NeXora;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *aiStart;
@end

@implementation Content

- (void)viewDidLoad {
    [super viewDidLoad];
    [self network];
    [self.aiStart startAnimating];
  
    
}

/*=============================
Check  the network
==============================*/
-(void)network
    {
        [[AFNetworkReachabilityManager sharedManager]startMonitoring];
        [[AFNetworkReachabilityManager sharedManager]setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
            if (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi) {
                
                //If Internet ok
                [self dataJesonFormUrl];
                self->_aiStart.alpha=0;
                 [self.aiStart stopAnimating];
                
            }else{
                
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error No Internet"
                                                                               message:@"Check Your Internet Connection."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }];
        
        
    }
    
/*=============================
//If network Get Data
==============================*/
-(void)dataJesonFormUrl
    {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:WEBPHOTOS parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            if (responseObject) {
                
                //filtre get the first 10 item
                self.NeXora = [responseObject subarrayWithRange:NSMakeRange(0, 10)];
                [self->_MyCollection reloadData];
            }
            
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            
            NSLog(@"Fatal Error");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"An error occurred try again."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }];
        
    }
    
    
#pragma mark - Table view data source
    
-(NSInteger)numberOfSectionsInTableView:(UICollectionView *)tableView
    {
        return 50;
    }
    
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return[self.NeXora count];
}
    
    
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    UICollectionViewFlowLayout *collectionLayout = [[UICollectionViewFlowLayout alloc] init];
    [collectionLayout setScrollDirection:UICollectionViewScrollDirectionVertical];


    static NSString *identifier = @"Cell";
    ModelContent *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    [self.MyCollection selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
    
    NSArray *data = [self. NeXora objectAtIndex:indexPath.row];
    
    //Get text
     cell.NeXText.text = [data valueForKey:@"title"];
    
    //Get Imagen from Url    
    NSString *urlString = [data valueForKey:@"thumbnailUrl"];;
    NSURLRequest *requestc = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [cell.NeXImage loadRequest:requestc];

  
  
    
    return cell;
}

@end
